#!/usr/bin/env python
'''async_process.py'''

import asyncio
import os
from pathlib import Path
from statistics import mean, stdev
import time
from zipfile import ZipFile


async def process_file(file):
    '''Return the sum of all the prime numbers in the txt file
    contained in the zip file `file`.

    Parameters
    ----------
    file: file like object containing one million prime numbers.

    Returns
    -------
    integer
    '''
    with ZipFile(file) as fz:           # Get a Zipfile object of `file`
        for fname in fz.namelist():     # Iterate over the files  in fz
            with fz.open(fname) as fh:  # Context manager for fz
                subtotal = 0
                for line in fh.readlines()[1:]:
                    subtotal += sum(int(i) for i in line.strip().split())
        return file.stem, subtotal


async def process_file_instrumented(file):
    '''Return the sum of all the prime numbers in the txt file
    contained in the zip file `file`.

    Parameters
    ----------
    file: file like object containing one million prime numbers.
    instrument: bool, indicate whether to print process information

    Returns
    -------
    integer
    '''
    print(f' ***Process id: {os.getpid()}, File: {file.stem}')
    with ZipFile(file) as fz:           # Get a Zipfile object of `file`
        for fname in fz.namelist():     # Iterate over the files  in fz
            with fz.open(fname) as fh:  # Context manager for fz
                subtotal = 0
                for line in fh.readlines()[1:]:
                    subtotal += sum(int(i) for i in line.strip().split())
        return file.stem, subtotal


async def process_directory(directory, print_result=False, instrument=False):
    '''Retrun a dictionary where the keys are the names of the files in
    directory, and the values are the sum of the numbers in the file.

    Parameters
    ----------
    direcotry: Path object which is a directory containging primes

    Returns
    -------
    dict
    '''
    sum_dict = {}
    processor = process_file_instrumented if instrument else process_file
    for zf in directory.iterdir():
        key, val = await processor(zf) # await the result
        sum_dict[key] = val
    if print_result:
        print(sum_dict)
    return sum_dict


async def time_async(func, arg_tuple, num_runs):
    '''Print the mean and standard deviation of running func with
    arg num_runs times.

    Parameters
    ----------
    func: asynchronous function
    arg_tuple: tuple of arguments to be passed to func
    num_runs: integer, number of times func should be run

    Returns
    -------
    None
    '''
    times = []
    for _ in range(num_runs):
        start = time.perf_counter()
        await(func(*arg_tuple))
        end = time.perf_counter()
        times.append(end - start)
    msg_part_1 = f'{mean(times)} s ± {stdev(times) * 1000} ms per loop'
    msg_part_2 = f'(mean ± std. dev. of {num_runs} runs, 1 loop each)'
    print(msg_part_1, msg_part_2)


if __name__ == '__main__':
    import argparse

    # setup parser
    DESC1 = 'Async program to process primes directory and return'
    DESC2 = 'a dictionary with the structure:'
    DESC3 = "{'primesi': sum of ith million prime numbers}"
    DESC = ' '.join((DESC1, DESC2, DESC3))
    parser = argparse.ArgumentParser(prog='async_process', description=DESC)
    parser.add_argument('-t', '--time', action='store_true',
                        help='time execution')
    parser.add_argument('-i', '--instrument', action='store_true',
                        help='run instrumented version')

    cli_args = parser.parse_args()

    # program proper
    PRIMES_DIR = Path('../primes')    # the primes directory as a Path object

    # asyncio functions and blocks must be run in an event loop.
    # We use the builtin asyncio.run method to run our async code.
    if cli_args.time:
        asyncio.run(time_async(process_directory, (PRIMES_DIR,), 7))
    elif cli_args.instrument:
        asyncio.run(process_directory(PRIMES_DIR,
                                      print_result=True,
                                      instrument=True))
    else:
        asyncio.run(process_directory(PRIMES_DIR, print_result=True))

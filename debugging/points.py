'''points.py
Making a point about debugging
'''


from math import hypot


class Point:
    '''Simple 2d point class'''
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Point(self.x+other.x, self.y+other.y)

    def mid_point(self, other):
        '''Return the mid-point between self and other'''
        mid_x = (self.x + other.x)/2
        mid_y = (self.y + other.y)/2
        return Point(mid_x, mid_y)

    def distance(self, other):
        '''Return the eucledian distance between self and other'''
        dx = self.x - other.x
        dy = self.y - other.y
        return hypot(dx, dy)

    def __str__(self):
        return f'Point({self.x}, {self.y})'

    def __repr__(self):
        return str(self)

def main():
    p1 = Point(2, 3)
    p2 = Point(5, 7)
    print(f'Distance between {p1} and {p2} = {p1.distance(p2)}')
    print(f'Midpoint betwen {p1} and {p2} = {p1.mid_point(p2)}')


if __name__ == '__main__':
    main()

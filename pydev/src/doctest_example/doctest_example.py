'''doctest_example.py a simple illustration of doctests.'''


def integral_part(x):
    '''Return the integral part of x.

    Parameter
    ---------
    x :  number

    Returns
    -------
    float

    Examples
    --------
    >>> integral_part(4)
    4
    >>> integral_part(3.56)
    3
    >>> integral_part(-1.2)
    -1
    '''
    return int(x // 1)


if __name__ == '__main__':
    import doctest
    doctest.testmod()

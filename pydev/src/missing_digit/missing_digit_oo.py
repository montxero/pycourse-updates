'''A class based implementation of the solution to the missing digit puzzle.

This solution showcases the following concepts:
 Inheretance
 Dunder methods
 Abstract base classes
 Dataclasses
 Class variables
 Class methods
 Static methods
'''


from abc import ABC, abstractmethod
from dataclasses import dataclass
from operator import add, sub, mul, floordiv


class Expression(ABC):
    '''Abstract base class for Expression'''
    @abstractmethod
    def evaluate(self):
        raise NotImplementedError

    @abstractmethod
    def contains_missing_digit(self) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __repr__(self):
        raise NotImplementedError

    @abstractmethod
    def __str__(self):
        raise NotImplementedError

    @abstractmethod
    def __eq__(self, other):
        raise NotImplementedError


@dataclass
class SingletonExpression(Expression):
    '''Simple class to hold values'''
    value: str

    def contains_missing_digit(self):
        '''return True off the missing digit is in self'''
        return 'x' in self.value

    def evaluate(self):
        return self.value

    def __repr__(self):
        return f"SingletonExpression('{self.value}')"

    def __str__(self):
        return self.value

    def __eq__(self, other):
        return self.value == other.value


class Operator:
    '''Class to hold operators'''
    operator_table = {'+': add, '-': sub, '*': mul, '/': floordiv}

    def __init__(self, op: str) -> None:
        if op not in Operator.operator_table:
            raise ValueError('Operator must be one of "+ -* /"')
        self.operator = op

    def __repr__(self):
        return f'Operator({self.operator})'

    def __str__(self):
        return self.operator

    def __eq__(self, other):
        return self.operator == other.operator

    def converse(self):
        '''Return the converse of self'''
        converse_table = {'+': '-',
                          '*': '/',
                          '/': '*',
                          '-': '+'}
        return Operator(converse_table[self.operator])

    def apply(self,
              v1: SingletonExpression,
              v2: SingletonExpression) -> Expression:
        '''Return a new SingletonExpression which is the result of
             v1.value operator v2.value
        '''
        a, b = v1.value, v2.value
        if a.isnumeric() and b.isnumeric():
            x, y = map(int, (a, b))
            new_val = Operator.operator_table[self.operator](x, y)
            return SingletonExpression(str(new_val))
        return CompoundExpression(v1, self, v2)


class CompoundExpression(Expression):
    '''Class for expressions of the form:
    SingletonExpression Operator SingletonExpression.
    '''
    def __init__(self,
                 v1: SingletonExpression,
                 operator: Operator,
                 v2: SingletonExpression) -> None:
        self.v1 = v1
        self.v2 = v2
        self.operator = operator

    def evaluate(self):
        '''Return an expression which holds te result of evaluating self.'''
        return self.operator.apply(self.v1, self.v2)

    def contains_missing_digit(self):
        '''Return True iff self contains a missing digit'''
        v1, v2 = self.v1, self.v2
        return v1.contains_missing_digit() or v2.contains_missing_digit()

    def __repr__(self):
        v1, v2, op = map(repr, (self.v1, self.v2, self.operator))
        return f'CompoundExpression({v1}, {op}, {v2})'

    def __str__(self):
        v1, v2, op = map(str, (self.v1, self.v2, self.operator))
        return f'{v1} {op} {v2}'

    def __eq__(self, other):
        if not isinstance(other, CompoundExpression):
            return False
        v1, v2, op1 = self.v1, self.v2, self.operator
        u1, u2, op2 = other.v1, other.v2, other.operator
        return (v1 == u1) and (v2 == u2) and (op1 == op2)


class Equation:
    '''Hold and manipulate equations'''
    msg = 'Normalise not implemented for equation with 2 compound expressions'

    def __init__(self, expr1: Expression, expr2: Expression) -> None:
        if isinstance(expr1, SingletonExpression) and isinstance(expr2, CompoundExpression):
            self.expr1 = expr1
            self.expr2 = expr2
        elif isinstance(expr1, CompoundExpression) and isinstance(expr2, SingletonExpression):
            self.expr1 = expr2
            self.expr2 = expr1

    def __eq__(self, other):
        if not isinstance(other, Equation):
            return False
        sex1 = self.expr1
        sex2 = self.expr2
        oex1 = other.expr1
        oex2 = other.expr2
        if (sex1 == oex1) and (sex2 == oex2):
            return True
        return (sex1 == oex2) and (sex2 == oex1)

    def __repr__(self):
        return f'Equation({repr(self.expr1)}, {repr(self.expr2)})'

    def __str__(self):
        return f'{str(self.expr1)} = {str(self.expr1)}'

    def normalise(self):
        '''Return an equation E which is equivalent to self and
        where the SingletonExpression with the missing digit is expr1 in E.
        '''
        expr1 = self.expr1
        expr2 = self.expr2
        if expr1.contains_missing_digit():
            return self
        v1 = expr2.v1
        v2 = expr2.v2
        op = expr2.operator
        if v1.contains_missing_digit():  # expr1 = num_x op v2
            c1 = CompoundExpression(expr1, op.converse(), v2)
            return Equation(v1, c1)
        # expr1 = v1 op num_x
        if op.operator in '-/':
            c1 = CompoundExpression(v1, op, expr1)
        else:
            c1 = CompoundExpression(expr1, op.converse(), v1)
        return Equation(v2, c1)

    @classmethod
    def from_equation_string(cls, equation):
        '''Construct a new equation from a string
        equation has the form:
                 num op num = num
        See problem specification for details.
        '''
        a, op, b, _, c = equation.split()
        v1, v2, v3 = map(SingletonExpression, (a, b, c))
        operator = Operator(op)
        cexp = CompoundExpression(v1, operator, v2)
        return cls(cexp, v3)

    @staticmethod
    def missing_digit(equation_string: str) -> int:
        '''return the missing digit in the equation given by equation_string'''
        return Equation.from_equation_string(equation_string).missing_digit_()

    def missing_digit_(self):
        '''Return the missing digit in self.'''
        new_eq = self.normalise()
        number_with_missing_digit = new_eq.expr1.value
        numeric_string = new_eq.expr2.evaluate().value
        for i, j in zip(number_with_missing_digit, numeric_string):
            if i == 'x':
                return j
        raise ValueError('Unknown digit not found')


def get_missing_digit(equation_string: str) -> int:
    '''return the missing digit in the equation given by equation_string'''
    return Equation.from_equation_string(equation_string).missing_digit_()

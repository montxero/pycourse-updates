"""Test suite for Missing Digit solution"""


import unittest
from missing_digit_oo import (
    SingletonExpression,
    Operator,
    CompoundExpression,
    Equation,
    get_missing_digit,
)


class SingletonExpressionTest(unittest.TestCase):
    x1 = SingletonExpression('x')
    x2 = SingletonExpression('x1')
    x3 = SingletonExpression('1x')
    x4 = SingletonExpression('0x3')
    x5 = SingletonExpression('30')
    x6 = SingletonExpression('444433423')

    def test_contains_missing_digit(self):
        self.assertTrue(SingletonExpressionTest.x1.contains_missing_digit())
        self.assertTrue(SingletonExpressionTest.x2.contains_missing_digit())
        self.assertTrue(SingletonExpressionTest.x3.contains_missing_digit())
        self.assertTrue(SingletonExpressionTest.x4.contains_missing_digit())
        self.assertFalse(SingletonExpressionTest.x5.contains_missing_digit())
        self.assertFalse(SingletonExpressionTest.x6.contains_missing_digit())

    def test_evaluate(self):
        self.assertEqual(SingletonExpressionTest.x1.evaluate(), 'x')
        self.assertEqual(SingletonExpressionTest.x2.evaluate(), 'x1')
        self.assertEqual(SingletonExpressionTest.x3.evaluate(), '1x')
        self.assertEqual(SingletonExpressionTest.x4.evaluate(), '0x3')
        self.assertEqual(SingletonExpressionTest.x5.evaluate(), '30')
        self.assertNotEqual(SingletonExpressionTest.x6.evaluate(), '44433423')


class OperatorTest(unittest.TestCase):
    def test_apply(self):
        plus, minus, times, divide = map(Operator, '+-*/')
        v1 = SingletonExpression('1')
        v2 = SingletonExpression('2')
        v3 = SingletonExpression('3')
        v4 = SingletonExpression('42')
        self.assertEqual(plus.apply(v1, v2), SingletonExpression('3'))
        self.assertEqual(times.apply(v4, v3), SingletonExpression('126'))
        self.assertEqual(divide.apply(minus.apply(v4, plus.apply(v1, v2)), v3),
                         SingletonExpression('13'))  # (42 - (1+2)) / 3 = 13
        v5 = SingletonExpression('x1')
        c1 = CompoundExpression(v2, plus, v5)
        self.assertEqual(plus.apply(v2, v5), c1)

    def converse(self):
        plus, minus, times, divide = map(Operator, '+-*/')
        self.assertEqual(minus.converse(), plus)
        self.assertEqual(plus.converse(), minus)
        self.assertEqual(times.converse(), divide)
        self.assertEqual(divide.converse(), times)


class CompoundExpressionTest(unittest.TestCase):
    def test_evaluate(self):
        plus, minus, times, divide = map(Operator, '+-*/')
        v1 = SingletonExpression('1')
        v2 = SingletonExpression('2')
        v3 = SingletonExpression('3')
        v4 = SingletonExpression('42')
        v5 = SingletonExpression('1x')
        e1 = CompoundExpression(v1, plus, v2)
        e2 = CompoundExpression(v3, times, SingletonExpression('14'))
        e3 = CompoundExpression(v5, divide, v3)
        e4 = CompoundExpression(v2, minus, v1)
        self.assertEqual(e1.evaluate(), v3)
        self.assertEqual(e2.evaluate(), v4)
        self.assertEqual(e3.evaluate(), e3)
        self.assertEqual(e4.evaluate(), v1)

    def test_contains_missing_digit(self):
        plus, minus, times, divide = map(Operator, '+-*/')
        v1 = SingletonExpression('1')
        v2 = SingletonExpression('2')
        v3 = SingletonExpression('3')
        v4 = SingletonExpression('42')
        v5 = SingletonExpression('1x')
        v6 = SingletonExpression('x')
        c1 = CompoundExpression(v1, plus, v2)
        c2 = CompoundExpression(v3, times, SingletonExpression('14'))
        c3 = CompoundExpression(v5, divide, v3)
        c4 = CompoundExpression(v4, minus, v6)
        self.assertFalse(c1.contains_missing_digit())
        self.assertTrue(c3.contains_missing_digit())
        self.assertFalse(c2.contains_missing_digit())
        self.assertTrue(c4.contains_missing_digit())


class EquationTest(unittest.TestCase):
    def test_from_equation_string(self):
        eq1_str = '4 - 2 = x'
        eq2_str = '1x0 * 12 = 1200'
        c1 = CompoundExpression(SingletonExpression('4'),
                                Operator('-'),
                                SingletonExpression('2'))
        c2 = CompoundExpression(SingletonExpression('1x0'),
                                Operator('*'),
                                SingletonExpression('12'))
        eq1 = Equation(c1, SingletonExpression('x'))
        eq2 = Equation(c2, SingletonExpression('1200'))
        self.assertEqual(Equation.from_equation_string(eq1_str), eq1)
        self.assertEqual(Equation.from_equation_string(eq2_str), eq2)

    def test_normalise(self):
        eq1 = Equation.from_equation_string('4 - 2 = x')
        a, b, c = [SingletonExpression(x) for x in ('4', '2', 'x')]
        self.assertEqual(eq1.normalise(),
                         Equation(CompoundExpression(a, Operator('-'), b), c))
        eq2 = Equation.from_equation_string('7x * 14 = 1064')
        a, b, c = [SingletonExpression(x) for x in ('7x', '14', '1064')]
        self.assertEqual(eq2.normalise(),
                         Equation(CompoundExpression(c, Operator('/'), b), a))


class MissingDigitTest(unittest.TestCase):
    def test_missing_digit_alone_on_rhs(self):
        expression_result_pairs = {'12 - 4 = x' : '8',
                                   '2 + 2 = x'  : '4',
                                   '9 / 3 = x' : '3'}
        for k, v in expression_result_pairs.items():
            self.assertEqual(get_missing_digit(k), v)

    def test_missing_digit_on_rhs(self):
        expression_result_pairs = {'15 * 25 = 3x5' : '7',
                                   '64 / 2 = 3x' : '2',
                                   '1937 + 221876 = 223x13' : '8',
                                   '39 / 3 = x3' : '1',
                                   '23 - 11 = x2' : '1'}
        for k, v in expression_result_pairs.items():
            self.assertEqual(get_missing_digit(k), v)

    def test_missing_digit_on_lhs(self):
        expression_result_pairs = {'1x0 * 12 = 1200' : '0',
                                   '3x / 3 = 13' : '9',
                                   '1937 + 2218x6 = 223813' : '7',
                                   '15 * x5 = 375' : '2',
                                   'x3 - 11 = 12' : '2'}
        for k, v in expression_result_pairs.items():
            self.assertEqual(get_missing_digit(k), v)


if __name__ == '__main__':
    unittest.main()
